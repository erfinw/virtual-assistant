import speech_recognition as sr
from gtts import gTTS
from lib import klikdokterApi
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

import alsaaudio


def speak(audioString):
    tts = gTTS(text=audioString, lang='id')
    tts.save("audio.mp3")

def answering(data):
    config = ConfigParser()
    config.read('lib/klikdokterConfig.ini')
    if "dimana kita" in data:
        data = data.split(" ")
        location = data[2]
        speak("Kita berada di,  " + location + " .")
        os.system("chromium-browser https://www.google.nl/maps/place/" + location + "/&amp;")
    if "bye" in data:
        speak("sampai jumpa dilain kesempatan.")
        # sys.exit(0)
    if ("apa kabar") in data:
        speak("Aku baik baik saja.")
    if ("jam berapa") in data:
        speak(ctime())
    if ("sakit") in data:    	
		api = klikdokterApi.request
		text = data
		action = config['conversation']['Action']
		context = data
		scenario = config['conversation']['Scenario']
		keyword = data
		sort =config['conversation']['Sort']
		speak(api(text, action, context, scenario, keyword, sort))


def recordAudio():
	# using pyaduio
	# try:
	# 	# Record Audio
	# 	r = sr.Recognizer()
	# 	with sr.Microphone() as source:
	# 		print("Say something!")
	# 		audio = r.listen(source)

	# except TypeError as e:
	# 	print('error: {}'.format(e))

	# using alsa		
	device = 'default'
	# Open the device in nonblocking capture mode. The last argument could
	# just as well have been zero for blocking mode. Then we could have
	# left out the sleep call in the bottom of the loop
	inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NONBLOCK, device=device)

	# Set attributes: Mono, 44100 Hz, 16 bit little endian samples
	inp.setchannels(1)
	inp.setrate(44100)
	inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)

	# The period size controls the internal number of frames per period.
	# The significance of this parameter is documented in the ALSA api.
	# For our purposes, it is suficcient to know that reads from the device
	# will return this many frames. Each frame being 2 bytes long.
	# This means that the reads below will return either 320 bytes of data
	# or 0 bytes of data. The latter is possible because we are in nonblocking
	# mode.
	inp.setperiodsize(160)
	try:
		# Record Audio
		r = sr.Recognizer()
		print("Say something!")
		audio = inp.read()

	except TypeError as e:
		print('error: {}'.format(e))

    # Speech recognition using Google Speech Recognition
	data = ""
	try:
	    # Uses the default API key
	    # To use another API key: `r.recognize_google(audio, key="AIzaSyDJIWpHBLyuK_6bXXexKS0MnJs7JgAsL_Y")`
	    data = r.recognize_google(audio,language="id")
	    print("You said: " + data)
	    
	except sr.UnknownValueError:
	    print("Google Speech Recognition could not understand audio")
	except sr.RequestError as e:
	    print("Could not request results from Google Speech Recognition service; {0}".format(e))
	    # raise

	return data