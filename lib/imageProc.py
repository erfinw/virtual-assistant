import pygame
import sys,os
import speech_recognition as sr
from gtts import gTTS
from lib import audioProc

class change_image():
# Pygame initialization of pygma and setting up of the screen size and color
	def __init__(self):
		pygame.init()
		pygame.display.set_caption("Image Viewer")

		# self.size=self.width,self.height=1000,700
		# self.screen=pygame.display.set_mode(self.size)
		# self.screen=pygame.display.set_mode((0, 0), pygame.FULLSCREEN)		
		modes = pygame.display.list_modes()
		self.screen=pygame.display.set_mode(max(modes), pygame.NOFRAME)		



#The actual funtion to load an image and blit on screen and the screen is frozen for 2 seconds, this gives the effect of slideshow. The time can be varied cby changing the wait time

	def update_image(self,name):
		modes = pygame.display.list_modes()
		self.name=name
		self.image=pygame.image.load(self.name)		
		self.image = pygame.transform.scale(self.image, max(modes))
		self.screen.blit(self.image,(0,0))
		pygame.display.flip()
		pygame.time.wait(70) # Use your custom time defaulted to 2 sec
		return True
		
# The path of the images are speficed and using listdir function os we get the list of images. Inorder to filter other file have used 'endswith' in this case i have used only *.jpg files. Then append them to list and pass them to update screen functionality.	
 
	def load_image(self,uSaid):
		sentence = list (uSaid)    
		approved = [',','.','a','b','e','f','i','l','m','o','p','u','v']

# select array conatain image approved
		sentence[:] = [sa for sa in sentence if any(sub in sa for sub in approved)]	    
		sentence_len = len(sentence)
		prefix = 'scene/w-'
		suffix = '.png'
		new_sentence = [prefix + letter + suffix for letter in sentence] 
# load BG
		modes = pygame.display.list_modes()
		bg = pygame.image.load("scene/bg.png")
		bg = pygame.transform.scale(bg, max(modes))
		self.screen.blit(bg, (0, 0))
		pygame.display.flip()
# Load Image lip Synch
		for content in new_sentence:
			self.update_image(content)
				
	def load_audio(self,audio_source):
		pygame.mixer.music.load(audio_source)
		pygame.mixer.music.play() # play once	

# the main function 
	def main(self):
		while True:
			#always listening
			data = audioProc.recordAudio()			
			audioProc.answering(data)

			for event in pygame.event.get():
				# if event.type == pygame.K_ESCAPE:
				if event.type == 2: # press any key to exit
					pygame.quit()
					sys.exit()

