from itertools import cycle

try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

import time
import os

class App(tk.Tk):
    '''Tk window/label adjusts to size of image'''
    def __init__(self, image_files, x, y, delay):
        # the root will be self
        tk.Tk.__init__(self)
        # set x, y position only
        self.geometry('+{}+{}'.format(x, y))
        self.delay = delay

        # bg = RBGAImage("scene/w-bg.png")
        # w_close = RBGAImage("scene/w-close.png")
        # bg.paste(w_close, (0, 0), w_close)
        # facepic = ImageTk.PhotoImage(bg)
        # label1 = Label(image=facepic)
        # label1.grid(row = 0, column = 0)


        # allows repeat cycling through the pictures
        # store as (img_object, img_name) tuple
        self.pictures = cycle((tk.PhotoImage(file=image), image) for image in image_files)       
        
        self.picture_display = tk.Label(self)
        self.picture_display.pack()

    def show_slides(self):
        global sentence_len
        global i

        '''cycle through the images and show them'''
        # next works with Python26 or higher
        img_object, img_name = next(self.pictures)
        self.picture_display.config(image=img_object)
        # shows the image filename, but could be expanded
        # to show an associated description of the image
        self.title(img_name)
        if i < sentence_len:
            self.after(self.delay, self.show_slides)
            i += 1

    def run(self):

        os.system("mpg321 output.mp3")
        self.mainloop()


def _sentence(_string):
    global sentence_len
    global i
    i=1

    # set milliseconds time between slides
    delay = 140

    # get a series of gif images you have in the working folder
    # or use full path, or set directory to where the images are

    sentence = list (_string)    
    approved = [',','a','b','e','f','i','l','m','o','p','u','v']

    # select array conatain image approved
    sentence[:] = [sa for sa in sentence if any(sub in sa for sub in approved)]
    
    sentence_len = len(sentence)
    print (sentence)
    
    prefix = 'scene/w-'
    suffix = '.png'
    new_sentence = [prefix + letter + suffix for letter in sentence] 
    # 'scene/w-close.png',
    # upper left corner coordinates of app window
    x = 100
    y = 50

    app = App(new_sentence, x, y, delay)
    app.show_slides()
    app.run()

_sentence("apa kabar?",)