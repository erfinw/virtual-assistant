#!/usr/bin/ev python3
# Requires PyAudio and PySpeech.
 
import speech_recognition as sr
from time import ctime
import time
import os
from gtts import gTTS
import sys


from itertools import cycle

try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk
import cv2


# class App(tk.Tk):
#     '''Tk window/label adjusts to size of image'''
#     def __init__(self, image_files, x, y, delay):
#         # the root will be self
#         tk.Tk.__init__(self)
#         # set x, y position only
#         self.geometry('+{}+{}'.format(x, y))
#         self.delay = delay

#         # bg = RBGAImage("scene/w-bg.png")
#         # w_close = RBGAImage("scene/w-close.png")
#         # bg.paste(w_close, (0, 0), w_close)
#         # facepic = ImageTk.PhotoImage(bg)
#         # label1 = Label(image=facepic)
#         # label1.grid(row = 0, column = 0)


#         # allows repeat cycling through the pictures
#         # store as (img_object, img_name) tuple
#         self.pictures = cycle((tk.PhotoImage(file=image), image) for image in image_files)
#         # for image in image_files:
#         #     self.pictures = cycle((tk.PhotoImage(file=image), image))
        
#         self.picture_display = tk.Label(self)
#         self.picture_display.pack()

#     def show_slides(self):
#         global sentence_len
#         global i

#         '''cycle through the images and show them'''
#         # next works with Python26 or higher
#         img_object, img_name = next(self.pictures)
#         self.picture_display.config(image=img_object)
#         # shows the image filename, but could be expanded
#         # to show an associated description of the image
#         self.title(img_name)
#         if i < sentence_len:
#             self.after(self.delay, self.show_slides)
#             i += 1

#     def run(self):
#         #add event listener to run speak
#         # speak("Hallo, ada yang bisa Vina bantu ?")
#         self.mainloop()


# def _sentence(_string):
#     global sentence_len
#     global i
#     i=1

#     # set milliseconds time between slides
#     delay = 140

#     # get a series of gif images you have in the working folder
#     # or use full path, or set directory to where the images are

#     sentence = list (_string)    
#     approved = [',','a','b','e','f','i','l','m','o','p','u','v']

#     # select array conatain image approved
#     sentence[:] = [sa for sa in sentence if any(sub in sa for sub in approved)]
    
#     sentence_len = len(sentence)
#     print (sentence)
    
#     prefix = 'scene/w-'
#     suffix = '.png'
#     new_sentence = [prefix + letter + suffix for letter in sentence] 
#     # 'scene/w-close.png',
#     # upper left corner coordinates of app window
#     x = 100
#     y = 50

#     app = App(new_sentence, x, y, delay)
#     app.show_slides()
#     app.run()

# # _sentence("apa kabar?",)
def main(_source):
    # var= 0
    while True:
        # print 'loading images...'
        # if var==0:
        img =  cv2.imread(_source)
            # var=var+1
        # else:
        #     img =  cv2.imread('scene/m-close.png')
        cv2.namedWindow("test", cv2.WND_PROP_FULLSCREEN)          
        cv2.setWindowProperty("test", cv2.WND_PROP_FULLSCREEN, 1)
        cv2.imshow("test",img)
        key=cv2.waitKey(2000) #milisecond
        if key==27:
            break

def speak(audioString):
    sentence = list (audioString)    
    approved = [',','a','b','e','f','i','l','m','o','p','u','v']

    # select array conatain image approved
    sentence[:] = [sa for sa in sentence if any(sub in sa for sub in approved)]    
    sentence_len = len(sentence)
    prefix = 'scene/w-'
    suffix = '.png'
    new_sentence = [prefix + letter + suffix for letter in sentence] 


    tts = gTTS(text=audioString, lang='id')
    tts.save("audio.mp3")
    os.system("mpg321 audio.mp3")

    for _source in new_sentence:
        main(_source)

def recordAudio():
    # Record Audio
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say something!")

        audio = r.listen(source)
 
    # Speech recognition using Google Speech Recognition
    data = ""
    try:
        # Uses the default API key
        # To use another API key: `r.recognize_google(audio, key="AIzaSyDJIWpHBLyuK_6bXXexKS0MnJs7JgAsL_Y")`
        data = r.recognize_google(audio,language="id")
        print("You said: " + data)
        
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
        raise
 
    return data
 
def nlp(data):
    if "dimana kita" in data:
        data = data.split(" ")
        location = data[2]
        speak("Kita berada di,  " + location + " .")
        os.system("chromium-browser https://www.google.nl/maps/place/" + location + "/&amp;")
    if "bye" in data:
        speak("sampai jumpa dilain kesempatan.")
        sys.exit(0)
    if ("apa kabar") in data:
        speak("Aku baik baik saja.")
    if ("jam berapa") in data:
        speak(ctime())

# _sentence("Hallo, ada yang bisa Vina bantu ?,")

# initialization Bona : roBOot kesehataN kitA
# time.sleep(2)
speak("Hallo, ada yang bisa Vina bantu ?")
# while 1:
#     data = recordAudio()
#     nlp(data)

