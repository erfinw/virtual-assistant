import sys
import pygame

pygame.init()
clock = pygame.time.Clock()

# Some surfaces which serve as the player images.
PLAYER_IMAGE = pygame.Surface((30, 50))  # Idle player image.
PLAYER_IMAGE.fill((30, 150, 230))
IMAGE1 = pygame.Surface((30, 55))
IMAGE1.fill((60, 150, 180))
IMAGE2 = pygame.Surface((30, 60))
IMAGE2.fill((90, 150, 130))
IMAGE3 = pygame.Surface((30, 65))
IMAGE3.fill((120, 150, 80))
IMAGE4 = pygame.Surface((30, 70))
IMAGE4.fill((150, 150, 30))
PLAYER1_IMAGES_RIGHT = [IMAGE1,IMAGE2,IMAGE3,IMAGE4]

anim_index = 0

while True:
    screen = pygame.display.set_mode((1080, 400))
    background_image = pygame.Surface(screen.get_size())
    background_image.fill((30, 30, 30))
    background_rect = background_image.get_rect()

    player_image = PLAYER_IMAGE
    player_rect = player_image.get_rect(x=100, y=225)

    while True:
        # Handle the events.
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        keys_pressed = pygame.key.get_pressed()
        if keys_pressed[pygame.K_d]:
            # Increment the index and change the player image.
            anim_index += 1
            anim_index %= len(PLAYER1_IMAGES_RIGHT)*4
            # * 4 in the line above and floor div 4 to slow the animation down.
            player_image = PLAYER1_IMAGES_RIGHT[anim_index//4]
        else:  # Switch back to the idle image.
            player_image = PLAYER_IMAGE
            # Reset the index, so that we don't start in the middle of the anim.
            anim_index = 0

        # Draw everything at the end.
        screen.blit(background_image, background_rect)
        # Just blit the current player image at the player_rect's topleft coords.
        screen.blit(player_image, player_rect)

        pygame.display.update()
        clock.tick(30)