import pygame
import sys,os
import speech_recognition as sr
from gtts import gTTS

class change_image():
# Pygame initialization of pygma and setting up of the screen size and color
	def __init__(self):
		pygame.init()
		pygame.display.set_caption("Image Viewer")

		# self.size=self.width,self.height=1000,700
		# self.screen=pygame.display.set_mode(self.size)
		# self.screen=pygame.display.set_mode((0, 0), pygame.FULLSCREEN)		
		modes = pygame.display.list_modes()
		self.screen=pygame.display.set_mode(max(modes), pygame.NOFRAME)		



#The actual funtion to load an image and blit on screen and the screen is frozen for 2 seconds, this gives the effect of slideshow. The time can be varied cby changing the wait time

	def update_image(self,name):
		modes = pygame.display.list_modes()
		self.name=name
		self.image=pygame.image.load(self.name)		
		self.image = pygame.transform.scale(self.image, max(modes))
		self.screen.blit(self.image,(0,0))
		pygame.display.flip()
		pygame.time.wait(70) # Use your custom time defaulted to 2 sec
		return True
		
# The path of the images are speficed and using listdir function os we get the list of images. Inorder to filter other file have used 'endswith' in this case i have used only *.jpg files. Then append them to list and pass them to update screen functionality.	
 
	def load_image(self,uSaid):
		sentence = list (uSaid)    
		approved = [',','.','a','b','e','f','i','l','m','o','p','u','v']

# select array conatain image approved
		sentence[:] = [sa for sa in sentence if any(sub in sa for sub in approved)]	    
		sentence_len = len(sentence)
		prefix = 'scene/w-'
		suffix = '.png'
		new_sentence = [prefix + letter + suffix for letter in sentence] 
# load BG
		modes = pygame.display.list_modes()
		bg = pygame.image.load("scene/bg.png")
		bg = pygame.transform.scale(bg, max(modes))
		self.screen.blit(bg, (0, 0))
		pygame.display.flip()
# Load Image lip Synch
		for content in new_sentence:
			self.update_image(content)
				
	def load_audio(self,audio_source):
		pygame.mixer.music.load(audio_source)
		pygame.mixer.music.play() # play once	

# the main function 
	def main(self):
		while True:
			data = recordAudio()
			nlp(data)
			for event in pygame.event.get():
				# if event.type == pygame.K_ESCAPE:
				if event.type == 2: # press any key to exit
					pygame.quit()
					sys.exit()

def speak(audioString):
    tts = gTTS(text=audioString, lang='id')
    tts.save("audio.mp3")

def nlp(data):
    if "dimana kita" in data:
        data = data.split(" ")
        location = data[2]
        speak("Kita berada di,  " + location + " .")
        os.system("chromium-browser https://www.google.nl/maps/place/" + location + "/&amp;")
    if "bye" in data:
        speak("sampai jumpa dilain kesempatan.")
        sys.exit(0)
    if ("apa kabar") in data:
        speak("Aku baik baik saja.")
    if ("jam berapa") in data:
        speak(ctime())

def recordAudio():
	try:
		# Record Audio
		r = sr.Recognizer()
		with sr.Microphone() as source:
			print("Say something!")
			audio = r.listen(source)

	except TypeError as e:
		print('error: {}'.format(e))
		

    # Speech recognition using Google Speech Recognition
	data = ""
	try:
	    # Uses the default API key
	    # To use another API key: `r.recognize_google(audio, key="AIzaSyDJIWpHBLyuK_6bXXexKS0MnJs7JgAsL_Y")`
	    data = r.recognize_google(audio,language="id")
	    print("You said: " + data)
	    
	except sr.UnknownValueError:
	    print("Google Speech Recognition could not understand audio")
	except sr.RequestError as e:
	    print("Could not request results from Google Speech Recognition service; {0}".format(e))
	    # raise

	return data
                               
if __name__=='__main__':
	sentence ="Hallo, ada yang bisa Vina bantu . "
	obj=change_image()
	speak(sentence)
	obj.load_audio('audio.mp3')
	obj.load_image(sentence)
	obj.main()

