from PIL import Image, ImageTk
from Tkinter import Tk, Label
import time

root = Tk()

def RBGAImage(path):
    return Image.open(path).convert("RGBA")
    
# fullscreenn
width, height = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (width, height))
# Remove border
# root.overrideredirect(1) 
root.bind("<Escape>", lambda e: e.widget.quit())

bg = RBGAImage("scene/bg.png")
w_close = RBGAImage("scene/m-close.png")
bg.paste(w_close, (0, 0), w_close)
facepic = ImageTk.PhotoImage(bg)
label1 = Label(image=facepic)
label1.grid(row = 0, column = 0)
root.mainloop()

#tinggal cari cara ganti2


# root = tk.Tk()
# root.attributes('-alpha', 0.0) #For icon
# #root.lower()
# root.iconify()
# window = tk.Toplevel(root)

# # fullscreenn
# width, height = root.winfo_screenwidth(), root.winfo_screenheight()
# window.geometry("%dx%d+0+0" % (width, height))

# window.overrideredirect(1) #Remove border
# #window.attributes('-topmost', 1)
# #Whatever buttons, etc 
# close = tk.Button(window, text = "Close Window", command = lambda: root.destroy())
# close.pack(fill = tk.BOTH, expand = 1)
# window.mainloop()



# import Tkinter
# import cv2
# import PIL.Image, PIL.ImageTk

# class App:
# 	def __init__(self, window, window_title, image_path="dw1.png"):
# 		self.window = window

# 		# Load an image using OpenCV
# 		self.cv_img = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)

# 		# Get the image dimensions (OpenCV stores image data as NumPy ndarray)
# 		self.height, self.width, no_channels = self.cv_img.shape

# 		# Create a canvas that can fit the above image
# 		self.canvas = Tkinter.Canvas(window, width = self.width, height = self.height)
# 		self.canvas.pack()

# 		# Use PIL (Pillow) to convert the NumPy ndarray to a PhotoImage
# 		self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(self.cv_img))

# 		# Add a PhotoImage to the Canvas
# 		self.canvas.create_image(0, 0, image=self.photo, anchor=Tkinter.NW)

# 		# Button that lets the user blur the image
# 		self.btn_blur=Tkinter.Button(window, text="Blur", width=50, command=self.blur_image)
# 		self.btn_blur.pack(anchor=Tkinter.CENTER, expand=True)

# 		#fullscreen
# 		self.window.overrideredirect(True)
# 		self.window.overrideredirect(False)
# 		self.window.attributes('-fullscreen',True)

# 		#press "esc" to quit
# 		self.window.bind("<Escape>", lambda e: e.widget.quit())

# 		self.window.mainloop()

#  # Callback for the "Blur" button
#  	def blur_image(self):
# 		self.cv_img = cv2.blur(self.cv_img, (3, 3))
# 		self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(self.cv_img))
# 		self.canvas.create_image(0, 0, image=self.photo, anchor=Tkinter.NW)

# # Create a window and pass it to the Application object
# App(Tkinter.Tk(), "Tkinter and OpenCV")
# ====================================
# import Tkinter as tk
# root = tk.Tk()
# root.attributes('-alpha', 0.0) #For icon
# #root.lower()
# root.iconify()
# window = tk.Toplevel(root)

# # fullscreenn
# width, height = root.winfo_screenwidth(), root.winfo_screenheight()
# window.geometry("%dx%d+0+0" % (width, height))

# window.overrideredirect(1) #Remove border
# #window.attributes('-topmost', 1)
# #Whatever buttons, etc 
# close = tk.Button(window, text = "Close Window", command = lambda: root.destroy())
# close.pack(fill = tk.BOTH, expand = 1)
# window.mainloop()